import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'; //引入element-ui库
import 'element-ui/lib/theme-chalk/index.css'; //引入element-ui样式

// 引入formCreate表单库
import formCreate from '@form-create/element-ui'
import FcDesigner from './assets/js/form-create-designer'



Vue.use(ElementUI)
Vue.use(formCreate)
Vue.use(FcDesigner)


// 引入代码编辑器
import jsonlint from 'jsonlint-mod';
import { codemirror } from 'vue-codemirror'
import 'codemirror/addon/lint/lint'
import 'codemirror/addon/lint/json-lint'
import 'codemirror/lib/codemirror.css'
import 'codemirror/addon/lint/lint.css'
import 'codemirror/addon/edit/matchbrackets.js'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/mode/meta.js'

Vue.prototype.$jsonlint=jsonlint
// Vue.prototype.codemirror=codemirror
Vue.use(codemirror)



import VForm from 'vform-builds'  //引入VForm库
import 'vform-builds/dist/VFormDesigner.css'  //引入VForm样式
Vue.use(VForm)  //全局注册VForm(同时注册了v-form-designer和v-form-render组件)



Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
