// 递归 查找 field slect 修改配置选项的值
/**
 * @param {*要递归的数据} data
 * @param {*field 查找对应的字段} value 
 * @param {*要修改的数据} updata 
 * @param {*要修改的数据的key（默认options）} updataKey 
 * @returns 
 */
export const getTreeItem = function (data, value, updata, updataKey) {
  var temp = []
  if (data.length > 0) {
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
      if (element.children.length > 0 && !(element.field)) {
        getTreeItem(element.children, value, updata, updataKey)
      } else {
        if (element.field == value) {
          element[updataKey || 'options'] = updata
        }
      }
      temp.push(element)
    }
  }

  return temp
}
/**
 * @param {*} content 复制板的内容
 * @returns  返回是true为操作成功，反之则相反
 */
//h5复制文本到剪切板
export const copyContentH5 = function (content) {
  var copyDom = document.createElement('div');
  copyDom.innerText = content;
  copyDom.style.position = 'absolute';
  copyDom.style.top = '0px';
  copyDom.style.right = '-9999px';
  document.body.appendChild(copyDom);
  //创建选中范围
  var range = document.createRange();
  range.selectNode(copyDom);
  //移除剪切板中内容
  window.getSelection().removeAllRanges();
  //添加新的内容到剪切板
  window.getSelection().addRange(range);
  //复制
  var successful = document.execCommand('copy');
  copyDom.parentNode.removeChild(copyDom);
  try {
    var msg = successful ? "successful" : "failed";
    console.log('Copy command was : ' + msg);
    return true
  } catch (err) {
    
    console.log('Oops , unable to copy!');
    return false
  }
}

/**
 * @param {*} res   为文件内容
 * @param {*} type  blob类型
 * @param {*} filetype  下载文件后缀
 * @param {*} filename  下载文件名称（默认当前时间戳+demo）
 */
export const download = function (res, type, filetype='text',filename) {
  // 创建blob对象，解析流数据
  const blob = new Blob([res], {
    // 如何后端没返回下载文件类型，则需要手动设置：
    //type: 'application/pdf;chartset=UTF-8' 表示下载文档为pdf，
    //如果是word则设置为msword，excel为excel
    //如果是 html 则设置为text/html
    type: type,
  })
  const a = document.createElement('a')
  // 兼容webkix浏览器，处理webkit浏览器中href自动添加blob前缀，默认在浏览器打开而不是下载
  const URL = window.URL || window.webkitURL
  // 根据解析后的blob对象创建URL 对象
  const herf = URL.createObjectURL(blob)
  // 下载链接
  a.href = herf
  // 下载文件名,如果后端没有返回，可以自己写a.download = '文件.pdf'
  if(!filename){
    //如果没有设置文件名称则是时间戳+demo 组合成的字符串
    filename=new Date().getTime()+'demo'
  }
  a.download = filename +'.'+filetype
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
  // 在内存中移除URL 对象
  window.URL.revokeObjectURL(herf)
}