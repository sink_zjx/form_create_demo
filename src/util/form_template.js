export default function (data, type = 'vue') {
  if (!data) return
  const ruleData=data.rule
  const optionsData=data.options

  console.log(ruleData,optionsData)
  if (type == 'vue') {
    return `<template>
    <form-create
      v-model="fapi"
      :rule="rule"
      :option="option"
      @submit="onSubmit"
    ></form-create>
  </template>
  
  <script>
  import formCreate from "@form-create/element-ui";
  
  export default {
    data () {
      return {
          fapi: null,
          rule: formCreate.parseJson('${ruleData}'),
          option: formCreate.parseJson('${optionsData}')
      }
    },
    methods: {
      onSubmit (formData) {
        console.log(formData)
        //todo 提交表单
      }
    }
  }
  </script>`
  } else if(type == 'html') {
    return `<!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">

  </head>
  <body>
  <div id="app">
    <form-create
    v-model="fapi"
    :rule="rule"
    :option="option"
    @submit="onSubmit"
  ></form-create>
  </div>
  <!-- import Vue.js 
  <script src="https://vuejs.org/js/vue.min.js"></script>
  -->
<script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.12/vue.min.js"></script>
<!-- import ElementUI -->
<script src="https://cdn.jsdelivr.net/npm/element-ui@2.8.2/lib/index.js"></script>
<!-- import form-create -->
<script src="https://unpkg.com/@form-create/element-ui/dist/form-create.min.js"></script>
  <script>
      new Vue({
        el: '#app',
        data: {
          fapi: null,
          rule: formCreate.parseJson('${ruleData}'),
          option: formCreate.parseJson('${optionsData}')
        },
        methods: {
          onSubmit(formData) {
            console.log(formData)
            //todo 提交表单
          }
        }
      })
    </script>
  </body>
  </html>`
  }
}