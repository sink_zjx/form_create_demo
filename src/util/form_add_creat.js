
import FcDesigner from '@form-create/designer'
// 自定义 组件
export const setChack = () => {
  /**在组件里面注册 左侧自定义组件
   * import {setChack as checkbox} from '../util/form_add_creat'  //引入当前js 结构出setChack
   * 插入规则 写在钩子函数里面
   * //插入组件规则
   *  this.$refs.designer.addComponent(checkbox())
   * //插入拖拽按钮到`main`分类下
   *  this.$refs.designer.appendMenuItem('main', {
   *    icon: checkbox().icon,
   *    name: checkbox().name,
   *    label: checkbox().label,
   * })
   * 
   * 
   */
  const label = ' zjx多选框'
  const name = 'checkbox'
  let i = 1
  const uniqueId = () => `uni${i++}`
  const checkbox = {
    icon: 'icon-checkbox',
    label,
    name,
    rule: () => {
      return {
        type: name,
        field: uniqueId(),
        title: label,
        info: '',
        effect: {
          fetch: '',
        },
        props: {},
        options: [
          { value: '1', label: '选项1' },
          { value: '2', label: '选项2' },
        ],
      }
    },
    props: () => {
      return [
        //生成`checkbox`组件的`options`配置规则
        FcDesigner.makeOptionsRule('options'),
        {
          type: 'switch',
          field: 'type',
          title: '按钮类型',
          props: { activeValue: 'button', inactiveValue: 'default' },
        },
        { type: 'switch', field: 'disabled', title: '是否禁用' },
        {
          type: 'inputNumber',
          field: 'min',
          title: '可被勾选的 checkbox 的最小数量',
        },
        {
          type: 'inputNumber',
          field: 'max',
          title: '可被勾选的 checkbox 的最大数量',
        },
        {
          type: 'input',
          field: 'textColor',
          title: '按钮形式的 Checkbox 激活时的文本颜色',
        },
        {
          type: 'input',
          field: 'fill',
          title: '按钮形式的 Checkbox 激活时的填充色和边框色',
        },
      ]
    },
  }
  return checkbox
}

export const setTable = () => {
  console.log(FcDesigner.makeOptionsRule('options'));
  console.log(FcDesigner.makeOptionsRule('data'));
  console.log(FcDesigner);
  const label = ' zjx表格'
  const name = 'el-table'
  let i = 1
  const uniqueId = () => `uni${i++}`
  const table = {
    icon: 'icon-checkbox',
    label,
    name,
    rule: () => {
      return {
        type: name,
        field: uniqueId(),
        title: label,
        info: '',
        style: 'red',
        effect: {
          fetch: '',
        },
        props: {
          border:true,
          data: [{
            date: '2016-05-02',
            name: '王小虎',
            address: '上海市普陀区金沙江路 1518 弄'
          }, {
            date: '2016-05-04',
            name: '王小虎',
            address: '上海市普陀区金沙江路 1517 弄'
          }
          ],
          style:{
            width: '100%'
          }
        },
        children: [
          { type: 'el-table-column', props: { prop: 'date', label: '日期'} },
          { type: 'el-table-column', props: { prop: 'name', label: '名称' } },
          { type: 'el-table-column', props: { prop: 'address', label: '地址'}},
        ]
      }
    },
    props: () => {
      return [
        //生成`checkbox`组件的`options`配置规则
        FcDesigner.makeOptionsRule('data'),
        // {
        //   type: 'switch',
        //   field: 'type',
        //   title: '按钮类型',
        //   props: { activeValue: 'button', inactiveValue: 'default' },
        // },
        // { type: 'switch', field: 'disabled', title: '是否禁用' },
        // {
        //   type: 'inputNumber',
        //   field: 'min',
        //   title: '可被勾选的 checkbox 的最小数量',
        // },
        // {
        //   type: 'inputNumber',
        //   field: 'max',
        //   title: '可被勾选的 checkbox 的最大数量',
        // },
        // {
        //   type: 'input',
        //   field: 'textColor',
        //   title: '按钮形式的 Checkbox 激活时的文本颜色',
        // },
        // {
        //   type: 'input',
        //   field: 'fill',
        //   title: '按钮形式的 Checkbox 激活时的填充色和边框色',
        // },
      ]
    },
  }
  return table
}