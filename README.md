# form_create_demo

## 项目开始 
```
npm install
```

### 本地运行项目
```
npm run serve
```

### 项目打包 线上静态资源文件生成
```
npm run build
```


----- 

> 目录结构说明
```js
|-- FORM_CREATE_DEMO
    |-- .gitignore
    |-- babel.config.js
    |-- package-lock.json
    |-- package.json //项目配置文件
    |-- README.md
    |-- vue.config.js //vuecli配置
    |-- public //静态文件入口公共文件
    |   |-- demo.html  //测试demo
    |   |-- favicon.ico
    |   |-- index.html
    |-- src  
    |   |-- App.vue //项目根组件
    |   |-- main.js //入口文件
    |   |-- assets  //静态资源文件
    |   |   |-- logo.png
    |   |   |-- js
    |   |       |-- form-create-designer.js  //form-create-designer改写版
    |   |-- components //公共组件目录
    |   |   |-- HelloWorld.vue
    |   |-- router //路由
    |   |   |-- index.js
    |   |-- util  //工具文件
    |   |   |-- form_add_creat.js //form 左侧自定义组件 
    |   |   |-- form_template.js  //form 模板生成
    |   |   |-- form_template.md  //form模板的使用说明
    |   |   |-- form_util.js  //form的工具函数
    |   |-- views
    |       |-- About.vue
    |       |-- Home.vue //form页面
    |-- tests //测试工具文件
        |-- unit
            |-- example.spec.js

```
